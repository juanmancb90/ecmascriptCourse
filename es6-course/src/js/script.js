let name = "juan manuel";
const id = 1144034955;
let myAge = (y) => {
    let year = new Date();
    return year.getFullYear() - y;
};

console.log(`
    name => ${name}
    id   => ${id}
    age  => ${myAge(1990)}
`);

/*
import Rx from 'rxjs/Rx';

let observable = Rx.Observable;

let btn = document.getElementById("btn");
let handler = (e) => {
    console.log(`test`);
    btn.removeEventListener('click', handler);
};

btn.addEventListener('click', handler);

//let clicks = observable.fromEvent(btn, 'click');
*/