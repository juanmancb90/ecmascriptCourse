'use strict';

var _Rx = require('rxjs/Rx');

var _Rx2 = _interopRequireDefault(_Rx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var observable = _Rx2.default.Observable; /*let name = "juan manuel";
                                          const id = 1144034955;
                                          let myAge = (y) => {
                                              let year = new Date();
                                              return year.getFullYear() - y;
                                          };
                                          
                                          console.log(`
                                              name => ${name}
                                              id   => ${id}
                                              age  => ${myAge(1990)}
                                          `);
                                          */

var btn = document.getElementById("btn");
var handler = function handler(e) {
    console.log('test');
    btn.removeEventListener('click', handler);
};

btn.addEventListener('click', handler);

//let clicks = observable.fromEvent(btn, 'click');
