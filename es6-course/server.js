//dependencies
import express from 'express';
import cors from 'cors';
import path from 'path';

const app = express();

// config cors
app.use(cors());

/**
 * routing
 */
app.post('/api', (req, res) => {
	res.json({
		words: ["funny", "fat", "fast", "Time"]
	});
});

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
});


/*
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'dev') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Page Not Found, We Sorry :(');
  err.status = 404;
  next(err);
});
*/
//run server
app.listen(3030, () => {
	console.log('server listening on port 3030');
});
