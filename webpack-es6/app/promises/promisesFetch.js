import {log, logTitle} from '../logger';

export default () => {
    logTitle('Promises with Fetch');
    let apiPromises = fetch('http://api.icndb.com/jokes/random/5', {method: 'GET'});
    //log(apiPromises);
    apiPromises
    .then(response => {
        console.log(response);
        return response.json();
    })
    .then(data => {
        data.value.forEach( (val) => {
            log(`${val.joke}`);
        });
    })
    .catch(error => {
        log(error);
    });
}
