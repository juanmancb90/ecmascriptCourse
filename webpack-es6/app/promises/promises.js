import {log, logTitle} from '../logger';

export default () => {
    logTitle('Promises and promises All');
    let promrequest = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("promise rocks lml");
        }, 7000);

        setTimeout(() => {
            reject("promise reject :( ");
        }, 9000);
    });

    promrequest
    .then((response) => {
        log(response);
    })
    .catch((error) => {
        log(error);
    });

    let namesPromises = new Promise((resolve, reject) => {
        let names = ['juan', 'manuel', "carolina", "daniela", "yury"];
        setTimeout(() => {
            reject("error fuck");
        }, 5000);

        setTimeout(() => {
            resolve(names);
        }, 2000);
    });

    let lastnamesPromises = new Promise((resolve, reject) => {
        let lastnames = ['ssss', 'maaaaa', "casdsadasd", "danielaasdasd", "yuryasdasd"];
        setTimeout(() => {
            reject("error fuck");
        }, 6000);
        setTimeout(() => {
            resolve(lastnames);
        }, 3000);
    });

    Promise
    .all([namesPromises, lastnamesPromises])
    .then((data) => {
        let [n, l] = data;
        console.log(`${data}`);
        log(n);
        log(l);
    })
    .catch(error => {
        log(error);
    });

}
