import promisesFn from './promises';
import promisesFetch from './promisesFetch';

export default () => {
    setTimeout(() => {promisesFn()},5000);
    setTimeout(() => {promisesFetch()},10000);
};
