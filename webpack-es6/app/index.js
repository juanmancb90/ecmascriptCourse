import singleDefaultExports from './modules/singleDefaultExports/index';
import multipleDefaultExports from './modules/multipleNameExports/index';
import newSyntax from './newSyntax/index';
import funcObjtMethods from './func-methods-objects/index';
import promisesEx from './promises/index';
import classesEx from './classes/index';
import GeneratorsEX from './generators/index';
import mapSetEx from './maps/index';
import asyncEX from './async/index';
import asyncPREX from './asyncProgramming/index.js';
import reactApp from './reactapp/index';
import api from './modules/singleDefaultExports/api';
//import observerEX from './observable/index';
import {log, logTitle} from './logger';

class Index {
    constructor(){}
    local() {
        let name = "juan manuel";
        const id = 1144034955;
        let myAge = (y) => {
            let year = new Date();
            return year.getFullYear() - y;
        };
        logTitle('local');
        log(name);
        log(id);
        log(myAge(1990));
        api(name);
    }

    modules() {
        singleDefaultExports();
        multipleDefaultExports();
    }

    syntax() {
        newSyntax();
    }

    objects() {
        funcObjtMethods();
    }

    promises() {
        promisesEx();
    }

    exampleClasses() {
        classesEx();
    }

    generatorsExample() {
        GeneratorsEX();
    }

    mapAndSetExample() {
        mapSetEx();
    }

    asyncProgramming() {
        asyncPREX();
    }

    asyncExample() {
        asyncEX();
    }

    /*observerExJs() {
        observerEX();
    }*/
}
/*execute*/
var ctx = new Index();
ctx.local();
ctx.modules();
ctx.syntax();
ctx.objects();
ctx.promises();
ctx.exampleClasses();
ctx.generatorsExample();
ctx.mapAndSetExample();
ctx.asyncProgramming();
ctx.asyncExample();
//ctx.observerExJs();
reactApp();
