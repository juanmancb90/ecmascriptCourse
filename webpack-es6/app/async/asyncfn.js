import {log, logTitle} from '../logger';

export default () => {
    logTitle('Async asynchronous');
    async function getAsynchronous(handle) {
        let url = `https://api.github.com${handle}`;
        let response = await fetch(url);
        let data = await response.json();
        return data;
    }

    let getDataApi = async (handle) => {
        const [user, repos] = await Promise.all([
            getAsynchronous(`/users/${handle}`),
            getAsynchronous(`/users/${handle}/repos`)
       ]);
       
       log(`
            user: ${user.name}
            repos: ${repos.length}
       `);
        /*if (response.status === 200) {
            return body;
        } else {
            throw Error(body.message);
        }*/
    };
    
    getDataApi("juanmancb90");

    /*getDataApi("juanmancb90")
    .then((data) => {
        log(`response: ${data.name}`);
    })
    .catch((error) => {
        log(`${error.message}`);
    });

    class GitHubApi {
        async fetchUser(handle) {
            let url = `https://api.github.com/users/${handle}`;
            let response = await fetch(url);
            return await response.json();
        }
    }
    let ctx = new GitHubApi().fetchUser("juan")*/
};