import {log, logTitle} from '../logger';

export default () => {
    logTitle('for await loop');
    Symbol.asyncIterator = Symbol.asyncIterator || Symbol('asyncIterator');
    const delay = (ms) => {
        new Promise((resolve) => {
            setTimeout(resolve, ms);
        })
    };

    async function* getGenerator() {
        await delay(1000);
        yield 1;
        await delay(1000);
        yield 2;
        await delay(1000);
        yield 3;
    }

    async function main() {
        for await (const value of getGenerator()) {
            log(`${value}`);
        }
        /*const generator = getGenerator();
        while (true) {
            let {value, done} = await generator.next();
            if (done) {
                break;    
            }
            log(`${value}`);
        }*/
    }
    main();

};