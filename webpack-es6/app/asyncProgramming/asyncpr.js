import {log, logTitle} from '../logger';

export default () => {
    logTitle(`Array and Async Programming`);
    function getSymbolsForEach(stocks) {
        let symbols = [];
        stocks.forEach((stock) => {
            symbols.push(stock.symbol);
        });
        return symbols
    }

    function getSymbolsMap(stocks) {
        return stocks.map((stock) => {
            return stock.symbol;
        });
    }

    function getStockOver(stocks, min) {
        return stocks.filter((stock) => {
            return stock.price >= min;
        });
    }

    const symbolsForEach = getSymbolsForEach([
        {symbol: "XFX", price: 240.22, volume: 23432},
        {symbol: "TNZ", price: 332.19, volume: 5323},
        {symbol: "JXJ", price: 120.22, volume: 2353}
    ]);

    const symbolsMap = getSymbolsMap([
        {symbol: "XFX", price: 240.22, volume: 23432},
        {symbol: "TNZ", price: 332.19, volume: 5323},
        {symbol: "JXJ", price: 120.22, volume: 2353}
    ]);

    const symbolsFilter = getStockOver(
        [
            {symbol: "XFX", price: 240.22, volume: 23432},
            {symbol: "TNZ", price: 332.19, volume: 5323},
            {symbol: "JXJ", price: 120.22, volume: 2353}
        ],
        150.00
    );

    const symbolsFilterMap = [
        {symbol: "XFX", price: 240.22, volume: 23432},
        {symbol: "TNZ", price: 332.19, volume: 5323},
        {symbol: "JXJ", price: 120.22, volume: 2353}
    ];

    const stocksOverMapFiltered =
        symbolsFilterMap
            .filter((stock) => {
                return stock.price >= 150.00;
            })
            .map((stock) => {
                return stock.symbol;
            });

    log(`forEach() => ${JSON.stringify(symbolsForEach)}`);
    log(`map() => ${JSON.stringify(symbolsMap)}`);
    log(`filter() => ${JSON.stringify(symbolsFilter)}`);

    stocksOverMapFiltered.forEach((symbol) => {
        log(`mixin => ${JSON.stringify(symbol)}`);
    });

    let exchanges = [
        [
            {symbol: "XFX", price: 240.22, volume: 23432},
            {symbol: "TNZ", price: 332.19, volume: 5323}
        ],
        [
            {symbol: "JXJ", price: 120.22, volume: 2353},
            {symbol: "IZF", price: 140.22, volume: 6522}
        ]
    ];

    Array.prototype.concatAll = function()  {
        const rst = [];
        this.forEach((arr) => {
            arr.forEach((item) => {
                rst.push(item);
            });
        });
        return rst;
    };
    const concatStocks = exchanges.concatAll();
    concatStocks.forEach(stock => {
         log(`concatAll() => ${JSON.stringify(stock)}`);
    })

    var flattened = exchanges.reduce(( acc, cur ) => acc.concat(cur),[]);
    log(`concat with reduce() => ${JSON.stringify(flattened)}`);

    const languagesVotes = [
        "react",
        "angular",
        "angular",
        "vuejs",
        "react",
        "react",
        "react",
        "ember",
        "backbone",
        "vuejs",
        "vuejs",
        "vuejs"
    ];

    let initialValues = {};

    function reducer(tally, vote) {
        if (!tally[vote]) {
            tally[vote] = 1
        } else {
            tally[vote]++;
        }
        return tally
    }

    const result = languagesVotes.reduce(reducer, initialValues);
    log(`reduce() => ${JSON.stringify(result)}`);

    let objArr = [
        {
            "url": "htpp1",
            "rest-di": "dasdas1"
        },
        {
            "url": "htpp2",
            "rest-di": "dasdas2"
        },
        {
            "url": "htpp3",
            "rest-di": "dasdas3"
        },
        {
            "url": "htpp4",
            "rest-di": "dasdas4"
        },
        {
            "url": "htpp5",
            "rest-di": "dasdas5"
        }
    ];

    objArr.map(item => {
        //console.log(`${typeof item }\n ${JSON.stringify(item)}`);
        Object.entries(item).forEach(([k, v]) => {
            console.log(`${k} - ${v}`);
        });
    });

	logTitle(`advance flatering`)
    let advExchanges = [
        {
            name:   "NYSE",
            stocks: [
                {
					symbol: "XFX",
					closes: [
						{date: new Date(2014,11,24), price: 240.10},
						{date: new Date(2014,11,23), price: 250.10},
						{date: new Date(2014,11,22), price: 260.10}
					]
				},
				{
					symbol: "TNZ",
					closes: [
						{date: new Date(2014,11,24), price: 540.10},
						{date: new Date(2014,11,23), price: 550.10},
						{date: new Date(2014,11,22), price: 560.10}
					]
				}
            ]
		},
		{
            name:   "USD",
            stocks: [
                {
					symbol: "QWE",
					closes: [
						{date: new Date(2014,10,24), price: 340.10},
						{date: new Date(2014,10,23), price: 350.10},
						{date: new Date(2014,10,22), price: 360.10}
					]
				},
				{
					symbol: "QWW",
					closes: [
						{date: new Date(2014,10,24), price: 840.10},
						{date: new Date(2014,10,23), price: 850.10},
						{date: new Date(2014,10,22), price: 860.10}
					]
				}
            ]
		}
	];

	Array.prototype.concatAll = function() {
		var rst = [];
		this.forEach(function(subArry) {
			subArry.forEach(function(item) {
				rst.push(item);
			});
		});

		return rst;
	};

	let christmasEveCLoses =
		advExchanges.map(function(advExchanges) {
			return advExchanges.stocks.map(function(stock) {
				return stock.closes.filter(function(close) {
					return close.date.getMonth() === 11 && close.date.getDate() === 24;
						}).map((close) => {
							return {
								symbol: stock.symbol,
								price: close.price
							};
						});
				}).concatAll();
		}).concatAll();

		christmasEveCLoses.forEach(function(christmasEveCLoses) {
            log(`${JSON.stringify(christmasEveCLoses)}`);
            console.log(christmasEveCLoses);
		});
};
