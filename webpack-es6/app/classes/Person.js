import {log, logTitle} from '../logger';

class Person {
    constructor(nameuser, age) {
        this.name = nameuser;
        this.age = age;
    }

    toString() {
        log(`
            name: ${this.name}
            age:${this.age}
        `);
    }

    getName() {
        return this.name;
    }

    getAge() {
        return this.age;
    }

    static getObjectType() {
        log('static method person fuck yeah');
    }

    static upperCaseName(text) {
        return text.toUpperCase();
    }
}

export default Person;
