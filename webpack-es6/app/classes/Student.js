import Person from './Person';
import {log, logTitle} from '../logger';

class Student extends Person {
    constructor(name, grade){
        super(name);
        this.grade = grade;
    }

    getNameStudent(){
        let namePerson = super.getName();
        log(`
            person: ${namePerson}
            grade: ${this.grade}
        `);
    }
}

export default Student;
