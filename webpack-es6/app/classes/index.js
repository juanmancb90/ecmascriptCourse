import Person from './Person';
import Student from './Student';
import {log, logTitle} from '../logger';

export default () => {
    logTitle('Classes');
    let ctx = new Person("juan", 26);
    ctx.toString();
    log(ctx.getName());
    log(ctx.getAge());

    logTitle('static props class');
    Person.getObjectType();
    log(Person.upperCaseName("manuel"));

    logTitle('Extends Class');
    let student = new Student("juan", "university");
    student.getNameStudent();
};
