import maps from './maps';
import sets from './sets';

export default () => {
    maps();
    sets();
};
