import {log, logTitle} from '../logger';

export default () => {
    logTitle('Map');

    /*let maps = new Map();
    maps.set('name', "Juan manuel");
    maps.set('age', 33);
    maps.set('hobbies', "fuck and develop");*/
    let maps = new Map([['name', 'juan'],['age', 26], ['hobbies', 'study'], [{},{}]]);

    log(`
        maps  => ${JSON.stringify(maps)}
        size => ${maps.size}
        age => ${maps.get('age')}
        name => ${maps.get('name')}
    `);
    let deleteHobbies = maps.delete('hobbies');
    log(deleteHobbies);
    maps.forEach((k, v) => {
        log(`k: ${k} = ${v}`);
    });

    /*for ([key. val]] of maps) {
        log(`key: ${key}`);
        log(`${maps.keys()} ${map.values()}`);
    }*/
};
