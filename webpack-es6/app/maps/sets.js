import {log, logTitle} from '../logger';

export default () => {
    logTitle('Set');
    let setsEx = new Set(['javascript', 'python', 'php', null, {}]);
    let sets = new Set()
    .add('javascript')
    .add('python')
    .add('php')
    .add('java')
    .add('nodejs')
    .add('laravel')
    .add('reactjs')
    .add('vuejs');

    log(`${sets.size}`);
    let isJava = sets.delete('java');
    log(`java was delete: ${isJava} size: ${sets.size}`);
    sets.forEach(names => {log(`${names}`)});
    let isJavascriptContain = sets.has('javascript');
    log(`javascript is in sets: ${isJavascriptContain}`);

    let toArr = [...setsEx];
    log(`${toArr}`);

    let temp = [1,2,3,4,7,5,2,5,8,5,5,5];
    let numbers = [...new Set(temp)];
    log(numbers);
};
