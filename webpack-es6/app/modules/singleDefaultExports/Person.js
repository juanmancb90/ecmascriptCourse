class Person {
    constructor(nameuser) {
        this.name = nameuser;
    }

    toString() {
        console.log(`Class Person toString() => ${this.name}`);
    }
    
    getName() {
        return this.name;
    }
}

export default Person;
