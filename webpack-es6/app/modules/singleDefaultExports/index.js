import pi from './pi';
import Person from './Person';
import {log, logTitle} from '../../logger';

export default () => {
    logTitle("Single exports");
    log(pi);
    let person = new Person("Juan Manuel");
    person.toString();
    log(person.getName());
};
