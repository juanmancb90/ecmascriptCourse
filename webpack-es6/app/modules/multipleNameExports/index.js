import {add, mult, div, sub} from './math';
import {log, logTitle} from '../../logger';

export default () => {
    logTitle('Multiple Exports');
    log(add(10,20))
    log(sub(10, 20));
    log(mult(10, 20));
    log(div(10, 20));
};
