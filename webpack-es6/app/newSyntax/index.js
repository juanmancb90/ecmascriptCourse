import vars from './variables';
import operators from './operators';
import arrowsfn from './arrow';

export default () => {
    vars();
    operators();
    arrowsfn();
};
