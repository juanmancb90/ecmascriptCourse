import {log, logTitle} from '../logger';

export default () => {
    logTitle("() => {} arrow function");
    let raiz = a => {
        return Math.sqrt(a);
    };
    log(raiz(4));
    let add = (n1, n2) => {return n1+n2};
    log(add(20, 30));
}
