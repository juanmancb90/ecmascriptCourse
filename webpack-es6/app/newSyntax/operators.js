import {log, logTitle} from '../logger';

export default () => {
    logTitle('... spread operator');
    let names = ["juan", "manuel", "jhon"];
    let lastnames = ["chaguendo", "Bermeo", "developer"];
    let mixins = [...names, ...lastnames, "other"];
    log(mixins);
    let temp = [...names];
    log(temp);
    let fn = (n1, n2, n3) => {
        return n1+n2+n3;
    };
    let numbers = [10, 20, 633];
    let rst = fn(...numbers);
    log(rst);

    logTitle('spread operator objects');

    let personName = {
        firstName: "juan",
        lastName: "Chaguendo"
    };

    let address = {
        city: 'Cali',
        College: 'Univalle'
    };

    let person = {
        ...personName,
        ...address
    };

    log(`${JSON.stringify(person)}`);
};
