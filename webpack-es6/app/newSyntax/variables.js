import {log, logTitle} from '../logger';

export default () => {
    logTitle('var');
    var pi;
    if (true) {
        pi = 3.1416;
    }
    log(pi);
    logTitle('let');
    for (let i = 0; i < 10; i++) {
        log(i);
    }
    //log(i);
    logTitle('const');
    const name = "juan";
    log(name);
    const objMixins = {
        name: 'mixin',
        fn: () => {
            return true;
        }
    };
    log(JSON.stringify(objMixins));
}
