function log(content) {
    const context = document.querySelector("#second");
    const element = document.createElement('h4');
    const node = document.createTextNode(content);
    element.appendChild(node);
    element.className += "blue lighten-5";
    context.appendChild(element);
}

function logTitle(content) {
    const contextTitle = document.querySelector("#second");
    const elementTitle = document.createElement('h1');
    const nodeTitle = document.createTextNode(content);
    elementTitle.className += "blue lighten-1";
    elementTitle.appendChild(nodeTitle);
    contextTitle.appendChild(elementTitle);
}

export {log, logTitle};
