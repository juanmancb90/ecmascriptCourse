import {log, logTitle} from '../logger';
import Bluebird, {coroutine as co} from 'bluebird';

export default () => {
    logTitle('Generator with promises sexy code');
    let getRandomNumberPromise = new Promise((resolve, reject) => {
        setTimeout(() => {
            let randomNumber = Math.floor(Math.random() * 10) + 1;
            log(randomNumber)
            resolve(randomNumber);
        }, 2000);
    });

    let getJokesPromise = n => fetch(`http://api.icndb.com/jokes/random/${n}`, {method: 'GET'});

    /*getRandomNumberPromise.then(number => {
        getJokesPromise(number).then(response => {
            response.json().then(jokes => {
                let {value: jokesArr} = jokes;
                jokesArr.forEach((joke) => log(joke.joke));
            });
        })
    });*/

    co(function* () {
        let number = yield getRandomNumberPromise;
        let jokesResponse = yield getJokesPromise(number);
        let jokesJson = yield jokesResponse.json();
        let {value: jokesArr} = jokesJson;
        jokesArr.forEach((joke) => {
            log(joke.joke);
        });
    })();

    async function main() {
        log(`async working`);
        await Bluebird.delay(2000);
        console.log('done');
    }
    main();
};
