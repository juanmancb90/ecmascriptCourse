import {log, logTitle} from '../logger';

export default () => {
    logTitle('Generator function*');
    const gen = function* (){
        yield 1;
        yield 2;
        yield 3;
        yield true;
        yield null;
        yield {
            name: "juan"
        }
        return {
            done: "fuck true"
        }
    };

    let dataGen = gen();
    console.log(dataGen);
    log(JSON.stringify(dataGen.next().value));
    log(JSON.stringify(dataGen.next().value));
    log(JSON.stringify(dataGen.next().value));
    log(JSON.stringify(dataGen.next().value));
    log(JSON.stringify(dataGen.next().value));
    log(JSON.stringify(dataGen.next().value));
    log(JSON.stringify(dataGen.next().value));

    let eventLoopNumber = function* (arr) {
        for (let i = 0; i < arr.length; i++) {
            yield arr[i];
        }
    };

    let numberGen = eventLoopNumber([1,2,3,4,5]);
    let interval = setInterval(() => {
        const next = numberGen.next();
        if (next.done) {
            log("done");
            clearInterval(interval);
        } else {
            log(next.value);
        }
    }, 1000);
};
