import Generators from './generators';
import GeneratorsWithPromises from './generatorWithPromises';

export default () => {
    setTimeout(() => {Generators()},20000);
    setTimeout(() => {GeneratorsWithPromises()},30000);
};
