import React, { Component } from 'react';
import {coroutine as co} from 'bluebird';

class Section extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jokes: []
        };
    }

    componentDidMount() {
        console.log(`component mount`);
        co(function* () {
            let number = yield this.getRandomNumber();
            let jokesResponse = yield this.getJokes(number);
            let jokes = yield jokesResponse.json();
            let {value: jokesArr} = jokes;
            this.setState({jokes: jokesArr});
        }.bind(this))();
    }

    getRandomNumber() {
        let getRandomNumberPromise = new Promise((resolve, reject) => {
            setTimeout(() => {
                let randomNumber = Math.floor(Math.random() * 10) + 1;
                resolve(randomNumber);
            }, 1000);
        });
        return getRandomNumberPromise;
    }

    getJokes(n) {
        return fetch(`http://api.icndb.com/jokes/random/${n}`, {method: 'GET'});
    }

    render() {
        let {jokes: jokesArr} = this.state;
        var jokes = jokesArr.map((joke, key) => {
            return <li key={key}> { joke.joke } </li>
        });
        return (
            <div>
                <h1> React jokes { this.state.jokes.length } </h1>
                <ol> { jokes }</ol>
            </div>
        );
    }
}

export default Section;
