import React from 'react';
import ReactDOM from 'react-dom';
import Section from './components/Section';

//functional staless component
export default () => {
    ReactDOM.render(
        <Section />,
        document.getElementById('main')
    );
};
