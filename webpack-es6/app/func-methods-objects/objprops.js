import {log, logTitle} from '../logger';

export default () => {
    logTitle('Enhanced Object Properties');

    let brand = "lastname";

    let person = (name, lastname) => {
        return {
            name: name,
            age: (year) => {
                let actual = new Date();
                return actual.getFullYear() - year;
            },
            [`${brand}`]: lastname
        };
    };

    let rst = person("juan", "chaguendo");
    log(rst.name);
    log(rst.age(1990));
    log(rst.lastname);
}
