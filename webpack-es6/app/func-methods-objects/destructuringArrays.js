import {log, logTitle} from '../logger';

export default () => {
    logTitle('Array Destructuring');
    let numbers = [1,2,3,4,5];
    let [one, , three, ...restnumber] = numbers;
    log(one);
    log(three);
    log(restnumber);
    let getName = () => {
        let names = ["juan", "manuel", "josh", "aquiles"];
        return names;
    }
    let [juan,,josh] = getName();
    log(juan);
    log(josh);
}
