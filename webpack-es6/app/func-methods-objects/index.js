import enhancedObjectProperties from './objprops';
import destructArr from './destructuringArrays';
import destructObj from './destructuringObjects';
import dft from './default.js';

export default () => {
    enhancedObjectProperties();
    destructArr();
    destructObj();
    dft();
};
