import {log, logTitle} from '../logger';

export default () => {
    logTitle('Objects Destructuring');
    let getData = () => {
        let data = {
            name: 'juan Manuel',
            response: {
                status: 200,
                message: "success",
                comp: {
                    error: "not",
                }
            },
            hobbies: ["read", "study", "to do love things"],
            showInfo: function() {
                this.hobbies.forEach(hobby => {
                    console.log(`${this.name} likes ${hobby}`);
                });
            }
        };
        return data;
    };

    let getDataArrObj = () => {
        const rst = [
            {
                name: 'juan Manuel',
                response: {
                    status: 200,
                    message: "success",
                    comp: {
                        error: "not",
                    }
                },
                hobbies: ["read", "study", "to do love things"],
                showInfo: function() {
                    this.hobbies.forEach(hobby => {
                        console.log(`${this.name} likes ${hobby}`);
                    });
                }
            },
            {
                name: 'developer',
                response: {
                    status: 200,
                    message: "success",
                    comp: {
                        error: "not",
                    }
                },
                hobbies: ["read", "study", "to do love things"],
                showInfo: function() {
                    this.hobbies.forEach(hobby => {
                        console.log(`${this.name} likes ${hobby}`);
                    });
                }
            }
        ];
        return rst;
    };

    let {name: nameUser, response: {status: state, comp: {error}}} = getData();
    log(nameUser);
    log(state);
    log(error);
    log('--------------->');

    for (let {name: nameUser, response: {status: state, comp: {error}}} of getDataArrObj()) {
        log(nameUser);
        log(state);
        log(error);
        log('--------------->');
    }

    let saveItem = ({nameUser, callback}) => {
        log(nameUser);
        callback();
    };

    saveItem({
        nameUser: "testing papi",
        callback: () => {
            log("save item success lml")
        }

    });
}
