import {log, logTitle} from '../logger';

export default () => {
    logTitle('Function Default Parameters');
    let calculatePay = (yearSalary, bonus = {
        employeeBonus: 0,
        teamBonus: 0
    }) => {
        return yearSalary + bonus.employeeBonus + bonus.teamBonus;
    };

    let employeeSalary = calculatePay(34000, {
        employeeBonus: 3400,
        teamBonus: 2500
    });
    log(employeeSalary);
}
