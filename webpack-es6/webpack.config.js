const path = require('path');
const webpack = require('webpack');

module.exports = {
    devtool: 'source-map',
    entry: [
        path.join(__dirname, 'app/index.js')
    ],
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.join(__dirname, 'app'),
                exclude: path.join(__dirname, 'node_modules'),
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                unused: true,
                dead_code: true,
                warnings: true,
                screw_ie8: true
            },
            compressor: {
                warnings: false
            },
            minimize: true,
            sourceMap: true
        })
    ],
    devServer: {
        port: 3000,
        contentBase: path.join(__dirname , 'build'),
        inline: true
    }
};
